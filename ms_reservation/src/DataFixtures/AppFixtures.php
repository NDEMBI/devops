<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Reservation;
use Symfony\Component\Validator\Constraints\DateTime;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i <3 ; $i++) {
            $reservationBdd = new Reservation();
            $reservationBdd->setNumberAdulte($i);
            $reservationBdd->setNumberChild(2+$i);
            $reservationBdd->setStatus(true);
            $reservationBdd->setDateStartReservation(new \DateTime());
            $reservationBdd->setDateEndReservation( new \DateTime());
            $reservationBdd->setIdOption($i);
            $reservationBdd->setIdClient($i+1);
            $reservationBdd->setIdHotel($i+2);
            $reservationBdd->setIdChambre($i+3);
            $reservationBdd->setPrixTotal($i+40258);            
            $manager->persist($reservationBdd); 
            # code...
        }
        $manager->flush();
    }
}
