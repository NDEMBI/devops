<?php

namespace App\Form;

use App\Entity\Reservation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numberAdulte')
            ->add('numberChild')
            ->add('status')
            ->add('dateStartReservation')
            ->add('dateEndReservation')
            ->add('idOption')
            ->add('idClient')
            ->add('idHotel')
            ->add('idChambre')
            ->add('prixTotal')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
