<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReservationRepository")
 */
class Reservation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @JMS\Serializer\Annotation\Type("integer")
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numberAdulte;

    /**
     * @JMS\Serializer\Annotation\Type("integer")
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numberChild;

    /**
     * @JMS\Serializer\Annotation\Type("boolean")
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status;

    /** 
     * @ORM\Column(type="datetime", nullable=true, options={"default": "CURRENT_TIMESTAMP"})
     */
    private $dateStartReservation;

    /** 
     * @ORM\Column(type="datetime", nullable=true, options={"default": "CURRENT_TIMESTAMP"})
     */
    private $dateEndReservation;

    /**
     * @JMS\Serializer\Annotation\Type("integer")
     * @ORM\Column(type="integer", nullable=true)
     */
    private $idOption;

    /**
     * @JMS\Serializer\Annotation\Type("integer")
     * @ORM\Column(type="integer", nullable=true)
     */
    private $idClient;

    /**
     * @JMS\Serializer\Annotation\Type("integer")
     * @ORM\Column(type="integer", nullable=true)
     */
    private $idHotel;

    /**
     * @JMS\Serializer\Annotation\Type("integer")
     * @ORM\Column(type="integer", nullable=true)
     */
    private $idChambre;

    /**
     * @JMS\Serializer\Annotation\Type("integer")
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prixTotal;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumberAdulte(): ?int
    {
        return $this->numberAdulte;
    }

    public function setNumberAdulte(?int $numberAdulte): self
    {
        $this->numberAdulte = $numberAdulte;

        return $this;
    }

    public function getNumberChild(): ?int
    {
        return $this->numberChild;
    }

    public function setNumberChild(?int $numberChild): self
    {
        $this->numberChild = $numberChild;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(?bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDateStartReservation(): ?\DateTimeInterface
    {
        return $this->dateStartReservation;
    }

    public function setDateStartReservation(?\DateTimeInterface $dateStartReservation): self
    {
        $this->dateStartReservation = $dateStartReservation;

        return $this;
    }

    public function getDateEndReservation(): ?\DateTimeInterface
    {
        return $this->dateEndReservation;
    }

    public function setDateEndReservation(?\DateTimeInterface $dateEndReservation): self
    {
        $this->dateEndReservation = $dateEndReservation;

        return $this;
    }

    public function getIdOption(): ?int
    {
        return $this->idOption;
    }

    public function setIdOption(?int $idOption): self
    {
        $this->idOption = $idOption;

        return $this;
    }

    public function getIdClient(): ?int
    {
        return $this->idClient;
    }

    public function setIdClient(?int $idClient): self
    {
        $this->idClient = $idClient;

        return $this;
    }

    public function getIdHotel(): ?int
    {
        return $this->idHotel;
    }

    public function setIdHotel(?int $idHotel): self
    {
        $this->idHotel = $idHotel;

        return $this;
    }

    public function getIdChambre(): ?int
    {
        return $this->idChambre;
    }

    public function setIdChambre(?int $idChambre): self
    {
        $this->idChambre = $idChambre;

        return $this;
    }

    public function getPrixTotal(): ?int
    {
        return $this->prixTotal;
    }

    public function setPrixTotal(?int $prixTotal): self
    {
        $this->prixTotal = $prixTotal;

        return $this;
    }
}
