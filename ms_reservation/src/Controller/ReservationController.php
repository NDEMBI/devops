<?php

namespace App\Controller;

use App\Entity\Reservation;
use App\Form\ReservationType;
use App\Repository\ReservationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/reservation")
 */
class ReservationController extends Controller
{
    /**
     * @Route("/", name="reservation_index", methods={"GET"})
     */
    public function indexReservationAction(ReservationRepository $reservationRepository): Response
    {
        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($reservationRepository->findAll(), 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/new", name="reservation_new", methods={"GET","POST"})
     */
    public function newReservationAction(Request $request): Response
    {
       try {
        //On recupère les données depuis le formulaire
        $data = $request->getContent();
        //On les décodent
        $reservation = $this->get('jms_serializer')->deserialize($data, Reservation::class, 'json');
        //On insert les infos dans la database      
        $em = $this->getDoctrine()->getManager();
        $em->persist($reservation);
        $em->flush();
                return new Response('la réservation a été créee avec succès', Response::HTTP_CREATED);
        } catch (NotFoundHttpException $e) {
            echo 'Exception reçue  : ',  $e->getMessage(), "\n";
        }

    }

    /**
     * @Route("/{id}", name="reservation_show", methods={"GET"})
     */
    public function showReservationAction(Reservation $reservation): Response
    {
        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($reservation, 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /**
     * @Route("/{id}/edit", name="reservation_edit", methods={"GET","POST"})
     */
    public function editReservationAction(Request $request, Reservation $reservation): Response
    {
        
        $em = $this->getDoctrine()->getManager();
        $reservationBdd = $em->getRepository('App\Entity\Reservation')->findOneBy(['id' => $reservation->getId()]);      

       if ($reservationBdd) {
            $data = $request->getContent();
            $reservation = $this->get('jms_serializer')->deserialize($data, Reservation::class, 'json');
            $reservationBdd->setNumberAdulte($reservation->getNumberAdulte());
            $reservationBdd->setNumberChild($reservation->getNumberChild());
            $reservationBdd->setStatus($reservation->getStatus());
            $reservationBdd->setDateStartReservation($reservation->getDateStartReservation());
            $reservationBdd->setDateEndReservation($reservation->getDateEndReservation());
            $reservationBdd->setIdOption($reservation->getIdOption());
            $reservationBdd->setIdClient($reservation->getIdClient());
            $reservationBdd->setIdHotel($reservation->getIdHotel());
            $reservationBdd->setIdChambre($reservation->getIdChambre());
            $reservationBdd->setPrixTotal($reservation->getPrixTotal());            
               
            $em->flush();
            return new Response("update succès", Response::HTTP_ACCEPTED);
       }
        return new Response("update echec", Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("/{id}", name="reservation_delete", methods={"DELETE"})
     */
    public function deleteReservationAction(Request $request, Reservation $reservation): Response
    {
            $em = $this->getDoctrine()->getManager();
            $em->remove($reservation);
            $em->flush();
            return new Response("delete succès", Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("/client/{id}", name="reservation_by_id_client", methods={"GET"})
     */
    public function getReservationByIdClientAction(ReservationRepository $reservationRepository, $id): Response
    {
        $serializer = $this->get('jms_serializer');      
        $data = $serializer->serialize($reservationRepository->findBy(['idClient' => $id]), 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    /**
     * @Route("/changestatus/{id}/{st}", name="changes_status_reservation", methods={"POST","GET"})
     */
    public function changeStatusReservationAction($id,$st)
    {
        $em = $this->getDoctrine()->getManager();
        $reservation = $em->getRepository('App\Entity\Reservation')->findOneBy(['id' => $id]); 
        $reservation->setStatus((boolean)$st);
        $em->persist($reservation);
        $em->flush();
        return new Response("Statut modifié avec succès");
    }
    
   
}
