<?php
//test CRUD API
namespace App\Tests\Controller;

use App\Entity\Reservation;
use App\Controller\ReservationController;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\CurlHttpClient;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
Use App\DataFixtures\AppFixtures;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\DateTime;


class ReservationControllerTest extends KernelTestCase

{ 
    /**
     * @var HttpClient
     */     
    private $clientHttp;

    /** 
     * @var Reservation     
     */
    private $reservations;

    /**
     * @var EntityManager
     */
    private $em;    

      public  function setUp():void
    {        
        $this->clientHttp = HttpClient::create();
        $kernel = self::bootKernel(); 
        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        
        $loader = new Loader();
        $loader->addFixture(new AppFixtures);
        //On vide les fixtures de la table
        $purger = new ORMPurger($this->em);
        $executor = new ORMExecutor($this->em, $purger);
        $executor->execute($loader->getFixtures());
        $this->reservations = $this->em->getRepository(Reservation::class)->findAll();        

    }

        public function testVerifyIfInitialisationObjectIsOk()
   {      
        $this->assertNotNull($this->clientHttp);
        $this->assertNotNull($this->em);         
        $this->assertSame(Reservation::class,get_class($this->reservations[0]));
   }


        public function testIndexReservation()
	{
        $url =self::bootKernel()->getContainer()->get('router')->generate('reservation_index');       
        $response =  $this->clientHttp->request('GET', 'http://localhost:8004'.$url);
        $this->assertEquals(3,count($this->reservations));        
		$this->assertEquals(200, $response->getStatusCode());
    }

        public function testShowReservation()
	{
               
        $response =  $this->clientHttp->request('GET', 'http://localhost:8004'."/reservation/".$this->reservations[0]->getId());
        $this->assertSame(Reservation::class,get_class($this->reservations[0]));      
		$this->assertEquals(200, $response->getStatusCode());
    }
    
        public function testNewReservation()
	{
        $url =self::bootKernel()->getContainer()->get('router')->generate('reservation_new'); 
        $httpClient = new CurlHttpClient();        
        
        $response =  $httpClient->request('POST',
         'http://localhost:8004'.$url,
         [
			'json' => [
                'number_adulte' => 89,
                'number_child' =>5,
                'status'=> true,
              //  'date_start_reservation'=> (new DateTime()),
              // 'date_end_reservation'=> (new DateTime()),
                'id_option'=>79,
                'id_client' =>5,
                'id_hotel'=> 78,
                'id_chambre'=> 87,
                'prix_total'=> 258.35

            ],
        ]);               
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals(4,count($this->em->getRepository(Reservation::class)->findAll()));
    } 

        public function testEditReservation()
	{
        $httpClient = new CurlHttpClient();

        $response =  $httpClient->request('POST',
         "http://localhost:8004/reservation/".$this->reservations[0]->getId()."/edit",
         [
			'json' => [
                'number_adulte' => 89,
                'number_child' =>5,
                'status'=> true,
               // 'date_start_reservation'=> (new \DateTime()),
               // 'date_end_reservation'=> (new \DateTime()),
                'id_option'=>79,
                'id_client' =>5,
                'id_hotel'=> 78,
                'id_chambre'=> 87,
                'prix_total'=> 258.35

            ],
		]);        
        $this->assertEquals(202, $response->getStatusCode());
        
    }
    
        public function testRemoveReservation()
    {        
        $httpClient = new CurlHttpClient();
        $response =  $httpClient->request('DELETE',
         "http://localhost:8004/reservation/".$this->reservations[0]->getId());

        $this->assertEquals(202, $response->getStatusCode());
        $this->assertEquals(2,count($this->em->getRepository(Reservation::class)->findAll()));  
    }
}