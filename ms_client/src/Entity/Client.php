<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @JMS\Serializer\Annotation\Type("string")
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $nameClient;

    /**
     * @JMS\Serializer\Annotation\Type("string")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $emailClient;

    /**
     * @JMS\Serializer\Annotation\Type("string")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phoneNumberClient;

    /**
     * @JMS\Serializer\Annotation\Type("string")
     * @ORM\Column(type="string", length=255)
     */
    private $cniClient;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameClient(): ?string
    {
        return $this->nameClient;
    }

    public function setNameClient(?string $nameClient): self
    {
        $this->nameClient = $nameClient;

        return $this;
    }

    public function getEmailClient(): ?string
    {
        return $this->emailClient;
    }

    public function setEmailClient(?string $emailClient): self
    {
        $this->emailClient = $emailClient;

        return $this;
    }

    public function getPhoneNumberClient(): ?string
    {
        return $this->phoneNumberClient;
    }

    public function setPhoneNumberClient(?string $phoneNumberClient): self
    {
        $this->phoneNumberClient = $phoneNumberClient;

        return $this;
    }

    public function getCniClient(): ?string
    {
        return $this->cniClient;
    }

    public function setCniClient(string $cniClient): self
    {
        $this->cniClient = $cniClient;

        return $this;
    }
}
