<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\ClientType;
use App\Repository\ClientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/client")
 */
class ClientController extends Controller
{
    /**
     * @Route("/", name="client_index", methods={"GET"})
     */
        public function indexClientAction(ClientRepository $clientRepository): Response
    {
        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($clientRepository->findAll(), 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/new", name="client_new", methods={"GET","POST"})
     */
    public function newClientAction(Request $request): Response
    {
       try {
        //On recupère les données depuis le formulaire
        $data = $request->getContent();
        //On les décodent
        $client = $this->get('jms_serializer')->deserialize($data, Client::class, 'json');
        //On insert les infos dans la database      
        $em = $this->getDoctrine()->getManager();
        $em->persist($client);
        $em->flush();
                return new Response('', Response::HTTP_CREATED);
        } catch (NotFoundHttpException $e) {
            echo 'Exception reçue  : ',  $e->getMessage(), "\n";
        }

    }

    /**
     * @Route("/{id}", name="client_show", methods={"GET"})
     */
    public function showClientAction(Client $client): Response
    {
        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($client, 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /**
     * @Route("/{id}/edit", name="client_edit", methods={"GET","POST"})
     */
    public function editClientAction(Request $request, Client $client): Response
    {
        
        $em = $this->getDoctrine()->getManager();
        $clientBdd = $em->getRepository('App\Entity\Client')->findOneBy(['id' => $client->getId()]);      

       if ($clientBdd) {
            $data = $request->getContent();
            $client = $this->get('jms_serializer')->deserialize($data, Client::class, 'json');
            $clientBdd->setNameClient($client->getNameClient());
            $clientBdd->setEmailClient($client->getEmailClient());
            $clientBdd->setPhoneNumberClient($client->getPhoneNumberClient());
            $clientBdd->setCniClient($client->getCniClient());
            $em->flush();
            return new Response("update succès", Response::HTTP_ACCEPTED);
       }
        return new Response("update echec", Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("/{id}", name="client_delete", methods={"DELETE"})
     */
    public function deleteClientAction(Request $request, Client $client): Response
    {
            $em = $this->getDoctrine()->getManager();
            $em->remove($client);
            $em->flush();
            return new Response("delete succès", Response::HTTP_ACCEPTED);
    }
}
