<?php

namespace App\DataFixtures;

use App\Entity\Client;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
                
        for ($i=0; $i < 3 ; $i++) {
            $client = new Client(); 
            $client->setNameClient("xelo".$i);
            $client->setEmailClient("xelo@email".$i."fr");
            $client->setPhoneNumberClient("xelo".$i);
            $client->setCniClient($i."PASSEPORT".$i);
            $manager->persist($client);
            
        }
            $manager->flush();

       
    }
}
