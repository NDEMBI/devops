<?php
//test CRUD API
namespace App\Tests\Controller;

use App\Entity\Client as Clients;
use App\Controller\ClientController;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\CurlHttpClient;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
Use App\DataFixtures\AppFixtures;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class ClientControllerTest extends KernelTestCase

{ 
    /**
     * @var HttpClient
     */     
    private $clientHttp;

    /** 
     * @var Clients     
     */
    private $clients;

    /**
     * @var EntityManager
     */
    private $em;    

      public  function setUp():void
    {        
        $this->clientHttp = HttpClient::create();
        $kernel = self::bootKernel(); 
        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        
        $loader = new Loader();
        $loader->addFixture(new AppFixtures);
        //On vide les fixtures de la table
        $purger = new ORMPurger($this->em);
        $executor = new ORMExecutor($this->em, $purger);
        $executor->execute($loader->getFixtures());
        $this->clients = $this->em->getRepository(Clients::class)->findAll();        

    }

        public function testVerifyIfInitialisationObjectIsOk()
   {      
        $this->assertNotNull($this->clientHttp);
        $this->assertNotNull($this->em);         
        $this->assertSame(Clients::class,get_class($this->clients[0]));
   }


        public function testIndexClient()
	{
        $url =self::bootKernel()->getContainer()->get('router')->generate('client_index');       
        $response =  $this->clientHttp->request('GET', 'http://localhost:8000'.$url);
        $this->assertEquals(3,count($this->clients));        
		$this->assertEquals(200, $response->getStatusCode());
    }

        public function testShowClient()
	{
               
        $response =  $this->clientHttp->request('GET', 'http://localhost:8000'."/client/".$this->clients[0]->getId());
        $this->assertSame(Clients::class,get_class($this->clients[0]));      
		$this->assertEquals(200, $response->getStatusCode());
    }
    
        public function testNewClient()
	{
        $url =self::bootKernel()->getContainer()->get('router')->generate('client_new'); 
        $httpClient = new CurlHttpClient();        
        
        $response =  $httpClient->request('POST',
         'http://localhost:8000'.$url,
         [
			'json' => [
                'name_client' => 'Malonga22',
                'email_client' =>'test',
                'phone_number_client'=>'54646444',
                'cni_client'=>'mololmoito'

            ],
        ]);               
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals(4,count($this->em->getRepository(Clients::class)->findAll()));
    } 

        public function testEditClient()
	{
        $httpClient = new CurlHttpClient();

        $response =  $httpClient->request('POST',
         "http://localhost:8000/client/".$this->clients[0]->getId()."/edit",
         [
			'json' => [
                'name_client' => 'Kata edit',
                'email_client' =>'test',
                'phone_number_client'=>'54646444',
                'cni_client'=>'POPOLOLO'

            ],
		]);        
        $this->assertEquals(202, $response->getStatusCode());
        
    }
    
        public function testRemoveClient()
    {        
        $httpClient = new CurlHttpClient();
        $response =  $httpClient->request('DELETE',
         "http://localhost:8000/client/".$this->clients[0]->getId());

        $this->assertEquals(202, $response->getStatusCode());
        $this->assertEquals(2,count($this->em->getRepository(Clients::class)->findAll()));  
    }
}