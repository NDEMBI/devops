<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Chambre; 

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i <3 ; $i++) {
            $chambre = new Chambre();
            $chambre->setNumberChambre($i);
            $chambre->setIdHotel($i);
            $chambre->setEtageChambre($i+1);
            $chambre->setNumberLitChambre($i+2);
            $chambre->setPrixChambre($i+400);
            $chambre->setDescriptionChambre("Excellente vue sur la mère");
            $chambre->setStatusChambre(true);
            $chambre->setCategorieChambre("Royale");
            $manager->persist($chambre); 
            # code...
        }
        
        $manager->flush();
    }
}
