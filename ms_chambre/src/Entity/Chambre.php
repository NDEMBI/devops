<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChambreRepository")
 */
class Chambre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numberChambre;

     /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $categorieChambre;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $idHotel;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $etageChambre;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numberLitChambre;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prixChambre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $descriptionChambre;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":"0"})
     */
    private $statusChambre;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumberChambre(): ?int
    {
        return $this->numberChambre;
    }

    public function setNumberChambre(?int $numberChambre): self
    {
        $this->numberChambre = $numberChambre;

        return $this;
    }

    public function getEtageChambre(): ?int
    {
        return $this->etageChambre;
    }

    public function setEtageChambre(?int $etageChambre): self
    {
        $this->etageChambre = $etageChambre;

        return $this;
    }

    public function getNumberLitChambre(): ?int
    {
        return $this->numberLitChambre;
    }

    public function setNumberLitChambre(int $numberLitChambre): self
    {
        $this->numberLitChambre = $numberLitChambre;

        return $this;
    }

    public function getDescriptionChambre(): ?string
    {
        return $this->descriptionChambre;
    }

    public function setDescriptionChambre(?string $descriptionChambre): self
    {
        $this->descriptionChambre = $descriptionChambre;

        return $this;
    }

    public function getStatusChambre(): ?bool
    {
        return $this->statusChambre;
    }

    public function setStatusChambre(bool $statusChambre): self
    {
        $this->statusChambre = $statusChambre;

        return $this;
    }

    /**
     * Get the value of prixChambre
     */ 
    public function getPrixChambre()
    {
        return $this->prixChambre;
    }

    /**
     * Set the value of prixChambre
     *
     * @return  self
     */ 
    public function setPrixChambre($prixChambre)
    {
        $this->prixChambre = $prixChambre;

        return $this;
    }

    /**
     * Get the value of idHotel
     */ 
    public function getIdHotel()
    {
        return $this->idHotel;
    }

    /**
     * Set the value of idHotel
     *
     * @return  self
     */ 
    public function setIdHotel($idHotel)
    {
        $this->idHotel = $idHotel;

        return $this;
    }

    /**
     * Get the value of categorieChambre
     */ 
    public function getCategorieChambre()
    {
        return $this->categorieChambre;
    }

    /**
     * Set the value of categorieChambre
     *
     * @return  self
     */ 
    public function setCategorieChambre($categorieChambre)
    {
        $this->categorieChambre = $categorieChambre;

        return $this;
    }
}
