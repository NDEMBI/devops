<?php
//test CRUD API
namespace App\Tests\Controller;

use App\Entity\Chambre;
use App\Controller\ClientController;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\CurlHttpClient;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
Use App\DataFixtures\AppFixtures;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class ChambreControllerTest extends KernelTestCase

{ 
    /**
     * @var HttpClient
     */     
    private $clientHttp;

    /** 
     * @var Chambre     
     */
    private $chambres;

    /**
     * @var EntityManager
     */
    private $em;    

      public  function setUp():void
    {        
        $this->clientHttp = HttpClient::create();
        $kernel = self::bootKernel(); 
        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        
        $loader = new Loader();
        $loader->addFixture(new AppFixtures);
        //On vide les fixtures de la table
        $purger = new ORMPurger($this->em);
        $executor = new ORMExecutor($this->em, $purger);
        $executor->execute($loader->getFixtures());
        $this->chambres = $this->em->getRepository(Chambre::class)->findAll();        

    }

        public function testVerifyIfInitialisationObjectIsOk()
   {      
        $this->assertNotNull($this->clientHttp);
        $this->assertNotNull($this->em);         
        $this->assertSame(Chambre::class,get_class($this->chambres[0]));
   }


        public function testIndexChambre()
	{
        $url =self::bootKernel()->getContainer()->get('router')->generate('chambre_index');       
        $response =  $this->clientHttp->request('GET', 'http://localhost:8001'.$url);
        $this->assertEquals(3,count($this->chambres));        
		$this->assertEquals(200, $response->getStatusCode());
    }

        public function testShowChambre()
	{
               
        $response =  $this->clientHttp->request('GET', 'http://localhost:8001'."/chambre/".$this->chambres[0]->getId());
        $this->assertSame(Chambre::class,get_class($this->chambres[0]));      
		$this->assertEquals(200, $response->getStatusCode());
    }
    
        public function testNewChambre()
	{
        $url =self::bootKernel()->getContainer()->get('router')->generate('chambre_new'); 
        $httpClient = new CurlHttpClient();        
        
        $response =  $httpClient->request('POST',
         'http://localhost:8001'.$url,
         [
			'json' => [
                'number_chambre' => 89,
                'etage_chambre' =>5,
                'number_lit_chambre'=>7,
                'prix_chambre'=>100005,
                'description_chambre'=>'lorem lorem ip sum ',
                'status_chambre'=>false,
                'id_hotel'=>2

            ],
        ]);               
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals(4,count($this->em->getRepository(Chambre::class)->findAll()));
    } 

        public function testEditChambre()
	{
        $httpClient = new CurlHttpClient();

        $response =  $httpClient->request('POST',
         "http://localhost:8001/chambre/".$this->chambres[0]->getId()."/edit",
         [
			'json' => [
                'number_chambre' => 9,
                'etage_chambre' =>5,
                'number_lit_chambre'=>7,
                'prix_chambre'=>10078005,
                'description_chambre'=>'lorem lorem ip sum ',
                'status_chambre'=>false,
                'id_hotel'=>2

            ],
		]);        
        $this->assertEquals(202, $response->getStatusCode());
        
    }
    
        public function testRemoveChambre()
    {        
        $httpClient = new CurlHttpClient();
        $response =  $httpClient->request('DELETE',
         "http://localhost:8001/chambre/".$this->chambres[0]->getId());

        $this->assertEquals(202, $response->getStatusCode());
        $this->assertEquals(2,count($this->em->getRepository(Chambre::class)->findAll()));  
    }
}