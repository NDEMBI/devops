<?php

namespace App\Controller;

use App\Entity\OptionReservation;
use App\Form\OptionType;
use App\Repository\OptionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/option")
 */
class OptionController extends Controller
{
    /**
     * @Route("/", name="option_index", methods={"GET"})
     */
    public function indexOptionAction(OptionRepository $optionRepository): Response
    {
        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($optionRepository->findAll(), 'json');        
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/new", name="option_new", methods={"GET","POST"})
     */
    public function newOptionAction(Request $request): Response
    {
       try {
        //On recupère les données depuis le formulaire
        $data = $request->getContent();
        //On les décodent
        $option = $this->get('jms_serializer')->deserialize($data, OptionReservation::class, 'json');
        //On insert les infos dans la database      
        $em = $this->getDoctrine()->getManager();
        $em->persist($option);
        $em->flush();
                return new Response('', Response::HTTP_CREATED);
        } catch (NotFoundHttpException $e) {
            echo 'Exception reçue  : ',  $e->getMessage(), "\n";
        }

    }

    /**
     * @Route("/{id}", name="option_show", methods={"GET"})
     */
    public function showOptionAction(OptionReservation $option): Response
    {
        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($option, 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /**
     * @Route("/{id}/edit", name="option_edit", methods={"GET","POST"})
     */
    public function editOptionAction(Request $request, OptionReservation $option): Response
    {
        
        $em = $this->getDoctrine()->getManager();
        $optionBdd = $em->getRepository(OptionReservation::class)->findOneBy(['id' => $option->getId()]);      

       if ($optionBdd) {
            $data = $request->getContent();
            $option = $this->get('jms_serializer')->deserialize($data, OptionReservation::class, 'json');
            $optionBdd->setNameOption($option->getNameOption());
            $optionBdd->setPriceOption($option->getPriceOption());  
            $em->flush();
            return new Response("update succès", Response::HTTP_ACCEPTED);
       }
        return new Response("update echec", Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("/{id}", name="option_delete", methods={"DELETE"})
     */
    public function deleteOptionAction(Request $request, OptionReservation $option): Response
    {
            $em = $this->getDoctrine()->getManager();
            $em->remove($option);
            $em->flush();
            return new Response("delete succès", Response::HTTP_ACCEPTED);
    }
}
