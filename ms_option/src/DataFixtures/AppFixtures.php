<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\OptionReservation; 

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i <3 ; $i++) {
            $optionBdd = new OptionReservation();
            $optionBdd->setNameOption("OUloulou".$i);
            $optionBdd->setPriceOption(25*$i);  
            $manager->persist($optionBdd);
            # code...
        }
        $manager->flush();
    }
}
