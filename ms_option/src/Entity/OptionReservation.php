<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OptionRepository")
 */
class OptionReservation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @JMS\Serializer\Annotation\Type("string")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nameOption;

    /**
     * @JMS\Serializer\Annotation\Type("float")
     * @ORM\Column(type="float", nullable=true)
     */
    private $priceOption;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameOption(): ?string
    {
        return $this->nameOption;
    }

    public function setNameOption(?string $nameOption): self
    {
        $this->nameOption = $nameOption;

        return $this;
    }

    public function getPriceOption(): ?float
    {
        return $this->priceOption;
    }

    public function setPriceOption(?float $priceOption): self
    {
        $this->priceOption = $priceOption;

        return $this;
    }
}
