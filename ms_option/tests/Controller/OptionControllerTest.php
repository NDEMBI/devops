<?php
//test CRUD API
namespace App\Tests\Controller;

use App\Entity\OptionReservation;
use App\Controller\ClientController;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\CurlHttpClient;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
Use App\DataFixtures\AppFixtures;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class OptionControllerTest extends KernelTestCase

{ 
    /**
     * @var HttpClient
     */     
    private $clientHttp;

    /** 
     * @var OptionReservation     
     */
    private $options;

    /**
     * @var EntityManager
     */
    private $em;    

      public  function setUp():void
    {        
        $this->clientHttp = HttpClient::create();
        $kernel = self::bootKernel(); 
        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        
        $loader = new Loader();
        $loader->addFixture(new AppFixtures);
        //On vide les fixtures de la table
        $purger = new ORMPurger($this->em);
        $executor = new ORMExecutor($this->em, $purger);
        $executor->execute($loader->getFixtures());
        $this->options = $this->em->getRepository(OptionReservation::class)->findAll();        

    }

        public function testVerifyIfInitialisationObjectIsOk()
   {      
        $this->assertNotNull($this->clientHttp);
        $this->assertNotNull($this->em);         
        $this->assertSame(OptionReservation::class,get_class($this->options[0]));
   }


        public function testIndexOption()
	{
        $url =self::bootKernel()->getContainer()->get('router')->generate('option_index');       
        $response =  $this->clientHttp->request('GET', 'http://localhost:8003'.$url);
        $this->assertEquals(3,count($this->options));        
		$this->assertEquals(200, $response->getStatusCode());
    }

        public function testShowOption()
	{
               
        $response =  $this->clientHttp->request('GET', 'http://localhost:8003'."/option/".$this->options[0]->getId());
        $this->assertSame(OptionReservation::class,get_class($this->options[0]));      
		$this->assertEquals(200, $response->getStatusCode());
    }
    
        public function testNewOption()
	{
        $url =self::bootKernel()->getContainer()->get('router')->generate('option_new'); 
        $httpClient = new CurlHttpClient();        
        
        $response =  $httpClient->request('POST',
         'http://localhost:8003'.$url,
         [
			'json' => [
                'name_option' => "TEst  blablbalbabla",
                'price_option' => 525.254

            ],
        ]);               
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals(4,count($this->em->getRepository(OptionReservation::class)->findAll()));
    } 

        public function testEditOption()
	{
        $httpClient = new CurlHttpClient();

        $response =  $httpClient->request('POST',
         "http://localhost:8003/option/".$this->options[1]->getId()."/edit",
         [
			'json' => [
                'name_option' => "TEst  modification",
                'price_option' => 525.254

            ],
		]);        
        $this->assertEquals(202, $response->getStatusCode());
        
    }
    
        public function testRemoveOptionReservation()
    {        
        $httpClient = new CurlHttpClient();
        $response =  $httpClient->request('DELETE',
         "http://localhost:8003/option/".$this->options[0]->getId());

        $this->assertEquals(202, $response->getStatusCode());
        $this->assertEquals(2,count($this->em->getRepository(OptionReservation::class)->findAll()));  
    }
}