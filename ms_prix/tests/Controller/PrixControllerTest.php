<?php
//test CRUD API
namespace App\Tests\Controller;

use App\Entity\Prix;
use App\Controller\ClientController;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\CurlHttpClient;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
Use App\DataFixtures\AppFixtures;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class PrixControllerTest extends KernelTestCase

{ 
    /**
     * @var HttpClient
     */     
    private $clientHttp;

    /** 
     * @var Prix     
     */
    private $prixs;

    /**
     * @var EntityManager
     */
    private $em;    

      public  function setUp():void
    {        
        $this->clientHttp = HttpClient::create();
        $kernel = self::bootKernel(); 
        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        
        $loader = new Loader();
        $loader->addFixture(new AppFixtures);
        //On vide les fixtures de la table
        $purger = new ORMPurger($this->em);
        $executor = new ORMExecutor($this->em, $purger);
        $executor->execute($loader->getFixtures());
        $this->prixs = $this->em->getRepository(Prix::class)->findAll();        

    }

        public function testVerifyIfInitialisationObjectIsOk()
   {      
        $this->assertNotNull($this->clientHttp);
        $this->assertNotNull($this->em);         
        $this->assertSame(Prix::class,get_class($this->prixs[0]));
   }


        public function testIndexPrix()
	{
        $url =self::bootKernel()->getContainer()->get('router')->generate('prix_index');       
        $response =  $this->clientHttp->request('GET', 'http://localhost:8005'.$url);
        $this->assertEquals(3,count($this->prixs));        
		$this->assertEquals(200, $response->getStatusCode());
    }

        public function testShowPrix()
	{
               
        $response =  $this->clientHttp->request('GET', 'http://localhost:8005'."/prix/".$this->prixs[0]->getId());
        $this->assertSame(Prix::class,get_class($this->prixs[0]));      
		$this->assertEquals(200, $response->getStatusCode());
    }
    
        public function testNewPrix()
	{
        $url =self::bootKernel()->getContainer()->get('router')->generate('prix_new'); 
        $httpClient = new CurlHttpClient();        
        
        $response =  $httpClient->request('POST',
         'http://localhost:8005'.$url,
         [
			'json' => [
                'id_chambre' => 89,
                'price_chambre' =>5,
                'reduction_saison'=>7

            ],
        ]);               
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals(4,count($this->em->getRepository(Prix::class)->findAll()));
    } 

        public function testEditPrix()
	{
        $httpClient = new CurlHttpClient();

        $response =  $httpClient->request('POST',
         "http://localhost:8005/prix/".$this->prixs[0]->getId()."/edit",
         [
			'json' => [
               'id_chambre' => 89,
                'price_chambre' =>5,
                'reduction_saison'=>7

            ],
		]);        
        $this->assertEquals(202, $response->getStatusCode());
        
    }
    
        public function testRemovePrix()
    {        
        $httpClient = new CurlHttpClient();
        $response =  $httpClient->request('DELETE',
         "http://localhost:8005/prix/".$this->prixs[0]->getId());

        $this->assertEquals(202, $response->getStatusCode());
        $this->assertEquals(2,count($this->em->getRepository(Prix::class)->findAll()));  
    }
}