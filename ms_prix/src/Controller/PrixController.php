<?php

namespace App\Controller;

use App\Entity\Prix as Prix;
use App\Form\PrixType;
use App\Repository\PrixRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @Route("/prix")
 */
class PrixController extends Controller
{
    /**
     * @Route("/", name="prix_index", methods={"GET"})
     */
    public function indexPrixAction(PrixRepository $prixRepository): Response
    {
        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($prixRepository->findAll(), 'json');        
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/new", name="prix_new", methods={"GET","POST"})
     */
    public function newPrixAction(Request $request): Response
    {
       try {
        //On recupère les données depuis le formulaire
        $data = $request->getContent();
        //On les décodent
        $prix = $this->get('jms_serializer')->deserialize($data, Prix::class, 'json');
        //On insert les infos dans la database      
        $em = $this->getDoctrine()->getManager();
        $em->persist($prix);
        $em->flush();
                return new Response('succes', Response::HTTP_CREATED);
        } catch (NotFoundHttpException $e) {
            echo 'Exception reçue  : ',  $e->getMessage(), "\n";
        }

    }

    /**
     * @Route("/{id}", name="prix_show", methods={"GET"})
     */
    public function showPrixAction(Prix $prix): Response
    {
        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($prix, 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /**
     * @Route("/{id}/edit", name="prix_edit", methods={"GET","POST"})
     */
    public function editPrixAction(Request $request, Prix $prix): Response
    {
        
        $em = $this->getDoctrine()->getManager();
        $prixBdd = $em->getRepository(Prix::class)->findOneBy(['id' => $prix->getId()]);      

       if ($prixBdd) {
            $data = $request->getContent();
            $prix = $this->get('jms_serializer')->deserialize($data, Prix::class, 'json');
            $prixBdd->setIdChambre($prix->getIdChambre());
            $prixBdd->setPriceChambre($prix->getPriceChambre());
            $prixBdd->setReductionSaison($prix->getReductionSaison());  
            $em->flush();
            return new Response("update succès", Response::HTTP_ACCEPTED);
       }
        return new Response("update echec", Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("/{id}", name="prix_delete", methods={"DELETE"})
     */
    public function deletePrixAction(Request $request, Prix $prix): Response
    {
            $em = $this->getDoctrine()->getManager();
            $em->remove($prix);
            $em->flush();
            return new Response("delete succès", Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("/update", name="update_price", methods={"GET","POST"})
     */
    public function updateBedRoomsPriceAction(ParameterBagInterface $params,PrixRepository $prixRepository){
        $clientHttp = HttpClient::create();
        $serializer = $this->get('jms_serializer');
        $reponse = new Response();
        $allprix = json_decode($serializer->serialize($prixRepository->findAll(), 'json'));     
        $bedRooms = json_decode($clientHttp->request('GET',"http://localhost:8001/chambre/")->getContent());

        foreach ($allprix as $prix) {
            foreach($bedRooms as $bed){
                if($prix->id_chambre == $bed->id){                    
                     $reponse = $clientHttp->request('POST',"http://localhost:8001/chambre/".$bed->id."/edit",['json' => [
                    'number_chambre' => $bed->number_chambre,
                    'id_hotel' => $bed->number_chambre,
                    'etage_chambre' => $bed->etage_chambre,
                    'number_lit_chambre' => $bed->number_lit_chambre,
                    'prix_chambre' => $prix->price_chambre,
                    'description_chambre' => $bed->description_chambre,
                    'status_chambre' => $bed->status_chambre]])->getContent();                                       
                }
            }
        }
        return new Response($reponse); 
       
    }
}
