<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Prix;
use Symfony\Component\Validator\Constraints\DateTime;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i <3 ; $i++) {
            $prix = new Prix();
            $prix->setIdChambre($i);
            $prix->setPriceChambre(85+$i);
            $prix->setReductionSaison(85+$i);                      
            $manager->persist($prix); 
            # code...
        }
        $manager->flush();
    }
}
