<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PrixRepository")
 */
class Prix
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $idChambre;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $priceChambre;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $reductionSaison;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdChambre(): ?int
    {
        return $this->idChambre;
    }

    public function setIdChambre(?int $idChambre): self
    {
        $this->idChambre = $idChambre;

        return $this;
    }

    public function getPriceChambre(): ?float
    {
        return $this->priceChambre;
    }

    public function setPriceChambre(?float $priceChambre): self
    {
        $this->priceChambre = $priceChambre;

        return $this;
    }

    public function getReductionSaison(): ?float
    {
        return $this->reductionSaison;
    }

    public function setReductionSaison(?float $reductionSaison): self
    {
        $this->reductionSaison = $reductionSaison;

        return $this;
    }
}
