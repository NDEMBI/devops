<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Client;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @Route("/gateway")
 */
class GatewayForMsController extends Controller
{
    /**
     * @Route("/", name="gateway_index", methods={"GET"})
     */
    public function indexGatewayAllRouteAction()
    { 
            $router = $this->get('router');    
            $routes = $router->getRouteCollection();
            $serializer = $this->get('jms_serializer');
    
        foreach ($routes as $key => $value) {        
		    $data[$key]['route'] = $value->getPath();
            $data[$key]['name'] = $key;
        }
        $response = new Response("Hello Bienvenue sur le microservice de reservation voici les differentes routes:
            ".$serializer->serialize($data, 'json'));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
      
    }

    /**
     * @Route("/services", name="gateway_all_services", methods={"GET"})
     */
    public function allMicroServiceAction(ParameterBagInterface $params )
    { 
        $serializer = $this->get('jms_serializer');
        $all_services_with_parameters = array();

        foreach ($params->get('all_services') as $service) {
            $all_services_with_parameters[$service] = ($params->get($service));            
        }
        $response = new Response($serializer->serialize($all_services_with_parameters, 'json'));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
      
    }

    /**
     * @Route("/services/{service}", name="get_all_elements_service", methods={"GET"})
     */
    public function proxyGetAllElementsIntoOneServiceAction(ParameterBagInterface $params, $service)
    { 
        if ($this->getIfServiceExist($params,$service)) {
            
           if (!$this->getIfServiceIsActivate($params, $service)) {        
                return new Response("ce service est temporaiment indisponible");
            }        
            $clientHttp = HttpClient::create();
            $response = new Response($clientHttp->request('GET', $this->getUrlForService($params,$service))->getContent());
            $response->headers->set('Content-Type', 'application/json');
           return $response;
        }else{
            return new Response("Ce service n'existe pas veuilez vérifier le chemin");
        }     
    }

    /**
     * @Route("/services/{service}/update", name="update_price_bed_room", methods={"POST"})
     */
    public function proxyUpdatePriceForBedRoomAction(ParameterBagInterface $params, $service)
    { 
        if ($this->getIfServiceExist($params,$service)) {
            
           if (!$this->getIfServiceIsActivate($params, $service)) {        
                return new Response("ce service est temporaiment indisponible");
            }        
            $clientHttp = HttpClient::create();
            $response = new Response($clientHttp->request('POST', $this->getUrlForService($params,$service)."/update")->getContent());
            $response->headers->set('Content-Type', 'application/json');
           return new Response($response);
        }else{
            return new Response("Ce service n'existe pas veuilez vérifier le chemin");
        }     
    }

    /**
     * @Route("/services/{service}/{id}", name="show_element_in_service", methods={"GET"})
     */
    public function proxyShowElementIntoOneServiceAction($service, $id,ParameterBagInterface $params)
    {   
        if ($this->getIfServiceExist($params,$service)) {
            
             if (!$this->getIfServiceIsActivate($params, $service)) {        
                return new Response("ce service est temporaiment indisponible");
            }           
            $clientHttp = HttpClient::create();
            $response = new Response($clientHttp->request('GET', $this->getUrlForService($params,$service)."/".$id)->getContent());
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }else{
            return new Response("Ce service n'existe pas veuilez vérifier le chemin");
        }     
    }

    /**
     * @Route("/services/{service}/{id}/edit", name="edit_element_in_service", methods={"POST", "GET"})
     */
    public function proxyEditElementIntoOneServiceAction($service, $id,ParameterBagInterface $params, Request $req)
    {   
        if ($this->getIfServiceExist($params,$service)) {

             if (!$this->getIfServiceIsActivate($params, $service)) {        
                return new Response("ce service est temporaiment indisponible");
            }  
            $data = json_decode($req->getContent(),true);            
            $clientHttp = HttpClient::create();   
                
             return new Response($clientHttp->request('POST', $this->getUrlForService($params,$service)."/".$id."/edit",
             ['json' => $data])->getContent()); 
        }else{
            return new Response("Ce service n'existe pas veuilez vérifier le chemin");
        }     
    }

    /**
     * @Route("/services/{service}/new", name="add_element_in_service", methods={"POST", "GET"})
     */
    public function proxyAddElementIntoOneServiceAction($service, ParameterBagInterface $params, Request $req)
    {   
        if ($this->getIfServiceExist($params,$service)) {

             if (!$this->getIfServiceIsActivate($params, $service)) {        
                return new Response("ce service est temporaiment indisponible");
            }              
            $data = json_decode($req->getContent(),true);            
            $clientHttp = HttpClient::create();          
                
             return new Response($clientHttp->request('POST', $this->getUrlForService($params,$service)."/new",
             ['json' => $data])->getContent()); 
        }else{
            return new Response("Ce service n'existe pas veuilez vérifier le chemin");
        }     
    }

    /**
     * @Route("/services/{service}/{id}", name="delete_element_in_service", methods={"DELETE"})
     */
    public function proxyDeleteElementIntoOneServiceAction($service,$id, ParameterBagInterface $params)
    {   
        if ($this->getIfServiceExist($params,$service)) {

             if (!$this->getIfServiceIsActivate($params, $service)) {        
                return new Response("ce service est temporaiment indisponible");
            }          
            $clientHttp = HttpClient::create();         
                
             return new Response($clientHttp->request('DELETE', $this->getUrlForService($params,$service)."/".$id)->getContent());
        }else{
            return new Response("Ce service n'existe pas veuilez vérifier le chemin");
        }     
    }

    /**
     * @Route("/reservation/catalogue", name="reservation_step_one", methods={"GET"})
     */
    public function catalogueHotelForReservationAction(ParameterBagInterface $params)
    {   
        return $this->proxyGetAllElementsIntoOneServiceAction($params, 'ms_hotel');
    }

    /**
     * @Route("/reservation/catalogue/hotel/{id}", name="reservation_step_two", methods={"GET"})
     */
    public function choiceHotelForReservationAction(ParameterBagInterface $params,$id)
    {   
        return $this->proxyShowElementIntoOneServiceAction('ms_hotel',$id, $params);
    }

    /**
     * @Route("/reservation/catalogue/hotel/{id}/chambres", name="reservation_step_three", methods={"GET"})
     */
    public function catalogueBedRoomsIntoHotelForReservationAction(ParameterBagInterface $params,$id)
    {   
        $clientHttp = HttpClient::create();
        $response = new Response($clientHttp->request('GET', $this->getUrlForService($params,'ms_chambre')."/hotel/".$id)->getContent());
        $response->headers->set('Content-Type', 'application/json');         
                
        return $response;
    }

    /**
     * @Route("/reservation/catalogue/hotel/{id}/chambres/{pk}", name="reservation_step_four", methods={"GET"})
     */
    public function choiceBedRoomForReservationAction(ParameterBagInterface $params,$id,$pk)    
    { 
        $chambres = json_decode($this->catalogueBedRoomsIntoHotelForReservationAction($params,$id)->getContent());
        $chambre = json_decode($this->proxyShowElementIntoOneServiceAction('ms_chambre',$pk, $params)->getContent());

        foreach ($chambres as $chambr) {
            if($chambr->id == $chambre->id){
                return  $this->proxyShowElementIntoOneServiceAction('ms_chambre',$pk, $params);
            }
        }
        return new Response("cette chambre n'existe pas dans cet hotel");
    }

    /**
     * @Route("/reservation/catalogue/hotel/{id}/chambres/{pk}/reserver", name="reservation_step_five", methods={"POST", "GET"})
     */
    public function makeReservationAction(ParameterBagInterface $params,$id,$pk)    
    { 
        $chambres = json_decode($this->catalogueBedRoomsIntoHotelForReservationAction($params,$id)->getContent());
        $chambre = json_decode($this->proxyShowElementIntoOneServiceAction('ms_chambre',$pk, $params)->getContent());

        foreach ($chambres as $chambr) {
            if($chambr->id == $chambre->id){
                //code for reservation
                
                return  $this->proxyShowElementIntoOneServiceAction('ms_chambre',$pk, $params);
            }
        }
        return new Response("cette chambre n'existe pas dans cet hotel");
    }

    /**
     * @Route("/reservation/client/{id}", name="reservation_step_seven", methods={"POST","GET"})
     */
    public function userGetReservationAction(ParameterBagInterface $params,$id)    
    { 
        $clientHttp = HttpClient::create();        
        $response = new Response($clientHttp->request('GET', $this->getUrlForService($params,'ms_reservation')."/client/".$id)->getContent());
        $response->headers->set('Content-Type', 'application/json'); 
        return $response;
    }

    /**
     * @Route("/reservation/catalogue/hotel/{id}/chambres/{pk}/reserver/{ud}", name="reservation_step_six", methods={"POST", "GET"})
     */
    public function finishReservationAction(ParameterBagInterface $params,$id,$pk,$ud, Request $req, \Swift_Mailer $mailer )    
    { 
        $clientHttp = HttpClient::create();
        $response = new Response();
        //$mailer = new Swift_Mailer();        
        $chambre = $this->makeReservationAction($params,$id,$pk)->getContent();

        if(json_decode($chambre)->status_chambre){             
            $client =  $clientHttp->request('GET', $this->getUrlForService($params,'ms_client')."/".$ud)->getContent();       
            $data = json_decode($req->getContent(),true);
        
            $response = $clientHttp->request('POST', $this->getUrlForService($params,'ms_reservation')."/new",
             ['json' => $this->constructReservationObject(json_decode($client),json_decode($chambre),$data)])->getContent();
             //On change le status de la chambre réservée
            $clientHttp->request('GET', $this->getUrlForService($params,'ms_chambre')."/changestatus/".json_decode($chambre)->id."/0");

            $this->sendMailAfterReservation("ndembi2xel@gmail.com", json_decode($client)->email_client, "Bonjour ".json_decode($client)->name_client." votre reservation a été éffectuée avec succès:",$chambre, $mailer);
         
        return new Response($response);

        }else{
            return new Response("Cette chambre est déjà réservée");
        }
        
    }

    /**
     * @Route("/reservation/client/{id}/{pk}/annuler", name="reservation_step_annulation", methods={"POST", "GET"})
     */
    public function userAnnulationReservationAction(ParameterBagInterface $params,$id,$pk)    
    { 
        $clientHttp = HttpClient::create();
        return new Response($clientHttp->request('GET', $this->getUrlForService($params,'ms_reservation')."/changestatus/".$pk."/0")->getContent());
    }

    public function getUrlForService(ParameterBagInterface $params, $service){

            $service_parameters [] = $params->get($service);        
            $service_url = "http://";
            for ($i=1; $i < 3 ; $i++) { 
                $service_url.= $service_parameters[0][$i];
            }
            return  $service_url."/".$service_parameters[0][0];            
    }

    public function getIfServiceIsActivate(ParameterBagInterface $params, $service){
        $service_parameters [] = $params->get($service);         
        return ($service_parameters[0][3]) ? true : false ;

    }

    public function getIfServiceExist(ParameterBagInterface $params, $service){
        return in_array($service, $params->get('all_services'), TRUE);
    }

    public function preTreatmentAnalysis(ParameterBagInterface $params, $service){
            
        if (!$this->getIfServiceIsActivate($params, $service)) {         
                return new Response("ce service est temporaiment indisponible");
            }
        
    }

    public function sendMailAfterReservation($emetteur,$mail_desti,$objet,$reservation, \Swift_Mailer $mailer){
            $message = (new \Swift_Message($objet))
            ->setFrom($emetteur)
            ->setTo($mail_desti)
            ->setBody(json_encode($reservation));

            $mailer->send($message);
    }

    public function constructReservationObject($client, $chambre,$data){

            if (!empty($data)) {
                return $reservationArray = [
                    'number_adulte' => $data['number_adulte'],
                    'number_child' =>$data['number_child'],
                    'status'=> $data['status'],
                   // 'date_start_reservation'=> (new \DateTime()),
                   // 'date_end_reservation'=> (new \Datetime($data['date_end_reservation'])),
                    'id_option'=>$data['id_option'],
                    'id_client' =>$client->id,
                    'id_hotel'=> $chambre->id_hotel,
                    'id_chambre'=> $chambre->id,
                    'prix_total'=>$data['prix_total']
                ];
            }else{
               return  $reservationArray = [
                    'number_adulte' =>1,
                    'number_child' =>0,
                    'status'=> true,
                   // 'date_start_reservation'=> (new \DateTime()),
                    'id_client' =>$client->id,
                    'id_hotel'=> $chambre->id_hotel,
                    'id_chambre'=> $chambre->id,
                    'prix_total'=> $chambre->prix_chambre
                ];
            }
                      
    }
   
}
