<?php
//test CRUD API
namespace App\Tests\Controller;

use App\Entity\Hotel;
use App\Controller\ClientController;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\CurlHttpClient;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
Use App\DataFixtures\AppFixtures;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class HotelControllerTest extends KernelTestCase

{ 
    /**
     * @var HttpClient
     */     
    private $clientHttp;

    /** 
     * @var Hotel     
     */
    private $hotels;

    /**
     * @var EntityManager
     */
    private $em;    

      public  function setUp():void
    {        
        $this->clientHttp = HttpClient::create();
        $kernel = self::bootKernel(); 
        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        
        $loader = new Loader();
        $loader->addFixture(new AppFixtures);
        //On vide les fixtures de la table
        $purger = new ORMPurger($this->em);
        $executor = new ORMExecutor($this->em, $purger);
        $executor->execute($loader->getFixtures());
        $this->hotels = $this->em->getRepository(Hotel::class)->findAll();        

    }

        public function testVerifyIfInitialisationObjectIsOk()
   {      
        $this->assertNotNull($this->clientHttp);
        $this->assertNotNull($this->em);         
        $this->assertSame(Hotel::class,get_class($this->hotels[0]));
   }


        public function testIndexHotel()
	{
        $url =self::bootKernel()->getContainer()->get('router')->generate('hotel_index');       
        $response =  $this->clientHttp->request('GET', 'http://localhost:8002'.$url);
        $this->assertEquals(3,count($this->hotels));        
		$this->assertEquals(200, $response->getStatusCode());
    }

        public function testShowHotel()
	{
               
        $response =  $this->clientHttp->request('GET', 'http://localhost:8002'."/hotel/".$this->hotels[0]->getId());
        $this->assertSame(Hotel::class,get_class($this->hotels[0]));      
		$this->assertEquals(200, $response->getStatusCode());
    }
    
        public function testNewHotel()
	{
        $url =self::bootKernel()->getContainer()->get('router')->generate('hotel_new'); 
        $httpClient = new CurlHttpClient();        
        
        $response =  $httpClient->request('POST',
         'http://localhost:8002'.$url,
         [
			'json' => [
                'number_hotel' => 89,
                'etage_hotel' =>5,
                'number_lit_hotel'=>7,
                'prix_hotel'=>100005,
                'description_hotel'=>'lorem lorem ip sum ',
                'status_hotel'=>false

            ],
        ]);               
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals(4,count($this->em->getRepository(Hotel::class)->findAll()));
    } 

        public function testEditHotel()
	{
        $httpClient = new CurlHttpClient();

        $response =  $httpClient->request('POST',
         "http://localhost:8002/hotel/".$this->hotels[0]->getId()."/edit",
         [
			'json' => [
                'number_hotel' => 9,
                'etage_hotel' =>5,
                'number_lit_hotel'=>7,
                'prix_hotel'=>10078005,
                'description_hotel'=>'lorem lorem ip sum ',
                'status_hotel'=>false

            ],
		]);        
        $this->assertEquals(202, $response->getStatusCode());
        
    }
    
        public function testRemoveHotel()
    {        
        $httpClient = new CurlHttpClient();
        $response =  $httpClient->request('DELETE',
         "http://localhost:8002/hotel/".$this->hotels[0]->getId());

        $this->assertEquals(202, $response->getStatusCode());
        $this->assertEquals(2,count($this->em->getRepository(Hotel::class)->findAll()));  
    }
}