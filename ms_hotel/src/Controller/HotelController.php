<?php

namespace App\Controller;

use App\Entity\Hotel;
use App\Form\HotelType;
use App\Repository\HotelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/hotel")
 */
class HotelController extends Controller
{
    /**
     * @Route("/", name="hotel_index", methods={"GET"})
     */
    public function indexHotelAction(HotelRepository $hotelRepository): Response
    {
        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($hotelRepository->findAll(), 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/new", name="hotel_new", methods={"GET","POST"})
     */
    public function newHotelAction(Request $request): Response
    {
       try {
        //On recupère les données depuis le formulaire
        $data = $request->getContent();
        //On les décodent
        $hotel = $this->get('jms_serializer')->deserialize($data, Hotel::class, 'json');
        //On insert les infos dans la database      
        $em = $this->getDoctrine()->getManager();
        $em->persist($hotel);
        $em->flush();
                return new Response('', Response::HTTP_CREATED);
        } catch (NotFoundHttpException $e) {
            echo 'Exception reçue  : ',  $e->getMessage(), "\n";
        }

    }

    /**
     * @Route("/{id}", name="hotel_show", methods={"GET"})
     */
    public function showHotelAction(Hotel $hotel): Response
    {
        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($hotel, 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /**
     * @Route("/{id}/edit", name="hotel_edit", methods={"GET","POST"})
     */
    public function editHotelAction(Request $request, Hotel $hotel): Response
    {
        
        $em = $this->getDoctrine()->getManager();
        $hotelBdd = $em->getRepository('App\Entity\Hotel')->findOneBy(['id' => $hotel->getId()]);      

       if ($hotelBdd) {
            $data = $request->getContent();
            $hotel = $this->get('jms_serializer')->deserialize($data, Hotel::class, 'json');
            $hotelBdd->setNameHotel($hotel->getNameHotel());
            $hotelBdd->setWebsiteHotel($hotel->getWebsiteHotel());
            $hotelBdd->setEmailHotel($hotel->getEmailHotel());
            $hotelBdd->setPhoneNumberHotel($hotel->getPhoneNumberHotel());
            $hotelBdd->setAddressHotel($hotel->getAddressHotel());
            $hotelBdd->setDescriptionHotel($hotel->getDescriptionHotel());
            $em->flush();
            return new Response("update succès", Response::HTTP_ACCEPTED);
       }
        return new Response("update echec", Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("/{id}", name="hotel_delete", methods={"DELETE"})
     */
    public function deleteHotelAction(Request $request, Hotel $hotel): Response
    {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hotel);
            $em->flush();
            return new Response("delete succès", Response::HTTP_ACCEPTED);
    }
}
