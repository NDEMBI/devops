<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Hotel;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i <3 ; $i++) {
            $hotelBdd = new Hotel();
            $hotelBdd->setNameHotel("Hotel mama".$i);
            $hotelBdd->setWebsiteHotel("www.hotel");
            $hotelBdd->setEmailHotel("email@email.fr".$i);
            $hotelBdd->setPhoneNumberHotel("2521555".$i+5);
            $hotelBdd->setAddressHotel("rue du pingouin");
            $hotelBdd->setDescriptionHotel("Lorem ipsummmipsummm
            ipsummmipsummm");
            $manager->persist($hotelBdd); 
            # code...
        }

        $manager->flush();
    }
}
