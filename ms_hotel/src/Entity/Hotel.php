<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HotelRepository")
 */
class Hotel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $nameHotel;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $websiteHotel;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $emailHotel;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $phoneNumberHotel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $addressHotel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descriptionHotel;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameHotel(): ?string
    {
        return $this->nameHotel;
    }

    public function setNameHotel(?string $nameHotel): self
    {
        $this->nameHotel = $nameHotel;

        return $this;
    }

    public function getWebsiteHotel(): ?string
    {
        return $this->websiteHotel;
    }

    public function setWebsiteHotel(?string $websiteHotel): self
    {
        $this->websiteHotel = $websiteHotel;

        return $this;
    }

    public function getEmailHotel(): ?string
    {
        return $this->emailHotel;
    }

    public function setEmailHotel(?string $emailHotel): self
    {
        $this->emailHotel = $emailHotel;

        return $this;
    }

    public function getPhoneNumberHotel(): ?string
    {
        return $this->phoneNumberHotel;
    }

    public function setPhoneNumberHotel(?string $phoneNumberHotel): self
    {
        $this->phoneNumberHotel = $phoneNumberHotel;

        return $this;
    }

    public function getAddressHotel(): ?string
    {
        return $this->addressHotel;
    }

    public function setAddressHotel(?string $addressHotel): self
    {
        $this->addressHotel = $addressHotel;

        return $this;
    }

    public function getDescriptionHotel(): ?string
    {
        return $this->descriptionHotel;
    }

    public function setDescriptionHotel(string $descriptionHotel): self
    {
        $this->descriptionHotel = $descriptionHotel;

        return $this;
    }
}
